from numpy import linspace, full, angle, abs, sqrt, pi as PI, concatenate, sin, log2
from numpy.random import permutation
from itertools import product


_S3 = sqrt(3)


def type_a_c2r(severity):
    v_res = 1 - severity
    return (v_res, 0), (v_res, 0), (v_res, 0)


def type_b_c2r(severity):
    return (1 - severity, 0), (0, 0), (0, 0)


def type_c_c2r(severity):
    v_res = 1 - severity
    phasor = -0.5+_S3*v_res*1j/2
    phase = angle(phasor)
    phase = phase - 2*PI/3
    v_res = abs(phasor)
    return (0, 0), (v_res, phase), (v_res, -phase)


def type_d_c2r(severity):
    v_res1 = 1 - severity
    phasor = -0.5*v_res1+_S3*1j/2
    phase = angle(phasor)
    phase = phase - 2*PI/3
    v_res2 = abs(phasor)
    return (v_res1, 0), (v_res2, phase), (v_res2, -phase)


def type_e_c2r(severity):
    v_res = 1 - severity
    return (0, 0), (v_res, 0), (v_res, 0)


def type_f_c2r(severity):
    v_res1 = 1 - severity
    phasor = _S3*1j/3 - 0.5*v_res1+_S3*v_res1*1j/6
    phase = angle(phasor)
    phase = phase - 2*PI/3
    v_res2 = abs(phasor)
    return (v_res1, 0), (v_res2, phase), (v_res2, -phase)


def type_g_c2r(severity):
    v_res1 = 1 - severity
    v_res_a = 2/3+ v_res1/3
    phasor = -1/3-1/6*v_res1+_S3*v_res1*1j/2
    phase = angle(phasor)
    phase = phase - 2*PI/3
    v_res2 = abs(phasor)
    return (v_res_a, 0), (v_res2, phase), (v_res2, -phase)


class _TYPE:
    NONE = 0
    INTERRUPTION = 1
    SAG = 2
    SWELL = 4
    TRANSIENT = 8


class _SAGtYPES:
    A = 1
    B = 2
    C = 4
    D = 8
    E = 16
    F = 32
    G = 64


class LABELS:
    type = _TYPE()
    sag_types = _SAGtYPES()


LLS = LABELS()


def get_c2r_by_type(type):
    if LLS.sag_types.A == type:
        return type_a_c2r
    elif LLS.sag_types.B == type:
        return type_b_c2r
    elif LLS.sag_types.C == type:
        return type_c_c2r
    elif LLS.sag_types.D == type:
        return type_d_c2r
    elif LLS.sag_types.E == type:
        return type_e_c2r
    elif LLS.sag_types.F == type:
        return type_f_c2r
    elif LLS.sag_types.G == type:
        return type_g_c2r
    else:
        raise ValueError("Unknown sag type.")


def get_fault_type_by_name(type_name):
    return _TYPE.__dict__[type_name.upper()]


def get_sag_type_by_name(type_name):
    return _SAGtYPES.__dict__[type_name.upper()]


def _slice_timepoints(timepoints, from_moment, to_moment):
    from_moment_idx = (abs(timepoints - from_moment)).argmin()
    to_moment_idx = (abs(timepoints - to_moment)).argmin()
    return timepoints[:from_moment_idx], \
           timepoints[from_moment_idx:(to_moment_idx + 1)], \
           timepoints[(to_moment_idx + 1):]


def _data_gen(timepoints, amplitude, w, phase):
    amplitudes = full(timepoints.shape, amplitude)
    return amplitudes * sin(timepoints * w + phase)


def _gen_one_phase_fault_signal(timepoints, from_moment, to_moment, frequency, amplitude_base, phase_base, amplitude_delta, phase_delta, label):
    pre_fault_timepoints, during_fault_timepoints, post_fault_timepoints = _slice_timepoints(timepoints, from_moment,
                                                                                             to_moment)
    pre_fault = _data_gen(pre_fault_timepoints, amplitude_base, frequency, phase_base)
    during_fault = _data_gen(during_fault_timepoints, amplitude_base * amplitude_delta, frequency, phase_base+phase_delta)
    post_fault = _data_gen(post_fault_timepoints, amplitude_base, frequency, phase_base)
    return concatenate([pre_fault, during_fault, post_fault]), \
        concatenate([full(pre_fault_timepoints.shape, LLS.type.NONE),
                    full(during_fault_timepoints.shape, label),
                    full(post_fault_timepoints.shape, LLS.type.NONE)])


def _gen_one_phase_no_fault_signal(timepoints, frequency, amplitude_base, phase_base):
    no_fault = _data_gen(timepoints, amplitude_base, frequency, phase_base)
    return no_fault, full(no_fault.shape, LLS.type.NONE)


def sag_fault(fault_subtype_lbl, sim_duration, freq_sample, freq_signal, from_moment, to_moment, rms, phase_offset, sag_severity):
    c2r = get_c2r_by_type(fault_subtype_lbl)
    if fault_subtype_lbl == LLS.sag_types.C:
        if sag_severity > 1 - 0.8640987597877147:
            lbl = fault_subtype_lbl
        else:
            lbl = LLS.type.NONE
    elif fault_subtype_lbl == LLS.sag_types.G:
        if sag_severity > 1 - 0.8791888434065215:
            lbl = fault_subtype_lbl
        else:
            lbl = LLS.type.NONE
    else:
        if sag_severity > 0.1:
            lbl = fault_subtype_lbl
        else:
            lbl = LLS.type.NONE
    sample_num = int(sim_duration * freq_sample + 1)
    sim_duration = sample_num / freq_sample
    w = 2 * PI * freq_signal
    timepoints = linspace(0, sim_duration, sample_num)
    a, b, c = c2r(sag_severity)
    va, la = _gen_one_phase_fault_signal(timepoints, from_moment, to_moment, w, rms, 0 + phase_offset, a[0], a[1], lbl)
    vb, lb = _gen_one_phase_fault_signal(timepoints, from_moment, to_moment, w, rms, 2 * PI / 3 + phase_offset, b[0], b[1], lbl)
    vc, lc = _gen_one_phase_fault_signal(timepoints, from_moment, to_moment, w, rms, -2 * PI / 3 + phase_offset, c[0], c[1], lbl)
    return (va/rms/2+0.5, vb/rms/2+0.5, vc/rms/2+0.5), la


def no_fault(sim_duration, freq_sample, freq_signal, rms, phase_offset):
    sample_num = int(sim_duration * freq_sample + 1)
    sim_duration = sample_num / freq_sample
    w = 2 * PI * freq_signal
    timepoints = linspace(0, sim_duration, sample_num)
    va, la = _gen_one_phase_no_fault_signal(timepoints, w, rms, 0 + phase_offset)
    vb, lb = _gen_one_phase_no_fault_signal(timepoints, w, rms, 2 * PI / 3 + phase_offset)
    vc, lc = _gen_one_phase_no_fault_signal(timepoints, w, rms, -2 * PI / 3 + phase_offset)
    return (va/rms/2+0.5, vb/rms/2+0.5, vc/rms/2+0.5), la


class TrainingDataGenerator:
    def __init__(self,
                 phases_init=(0, 6.258641617, 256),
                 signal_time_init=(0.1, 0.1, 1),
                 fault_durations_init=(0.02, 0.04),
                 sampling_frequencies_init=4096,
                 signal_frequencies_init=(50, 50, 1),
                 severities_init=(0, 1, 32),
                 rms_init=230,
                 fault_start_timepoint_init=0.04):
        """
        11469 je duzina svakog pojedinacnog uzorka kada je duzina trajanja signala 2.8 sekundi
        """
        self.b_no = 32  # batch_number
        self.configurations = []
        self.fault_start_timepoint = fault_start_timepoint_init
        types_not_a = [LLS.sag_types.B, LLS.sag_types.C, LLS.sag_types.D, LLS.sag_types.E, LLS.sag_types.F, LLS.sag_types.G]
        signal_times = linspace(signal_time_init[0], signal_time_init[1], signal_time_init[2])
        sampling_frequencies = [sampling_frequencies_init]
        signal_frequencies = linspace(signal_frequencies_init[0], signal_frequencies_init[1], signal_frequencies_init[2])
        rmss = [rms_init]
        phases = linspace(phases_init[0], phases_init[1], phases_init[2])
        no_faults = product([LLS.type.NONE], signal_times, sampling_frequencies, signal_frequencies, rmss, phases)
        fault_durations = list(fault_durations_init)
        severities = linspace(severities_init[0], severities_init[1], severities_init[2])
        sag_types_a = product([LLS.sag_types.A], signal_times, sampling_frequencies, signal_frequencies, [self.fault_start_timepoint], fault_durations, rmss, phases, severities)
        sag_types_not_a = product(types_not_a, signal_times, sampling_frequencies, signal_frequencies, [self.fault_start_timepoint], fault_durations, rmss, phases, severities, [0, 1, 2])
        self.configurations.extend(no_faults)
        self.configurations.extend(sag_types_a)
        self.configurations.extend(sag_types_not_a)
        self.configurations = permutation(self.configurations)
        self.len = len(self.configurations)
        print("Ukupan broj konfiguracija:", self.len)
        self.i = 0

    def __iter__(self):
        return self

    def __next__(self):
        batch_data = []
        batch_labels = []
        while self.i < self.len:
            config_one = self.configurations[self.i]
            if config_one[0] == LLS.type.NONE:
                data, labels = no_fault(config_one[1], config_one[2], config_one[3], config_one[4], config_one[5])
            elif config_one[0] == LLS.sag_types.A:
                data, labels = sag_fault(config_one[0], config_one[1], config_one[2], config_one[3], config_one[4], config_one[4]+config_one[5], config_one[6], config_one[7], config_one[8])
            else:
                data, labels = sag_fault(config_one[0], config_one[1], config_one[2], config_one[3], config_one[4], config_one[4]+config_one[5], config_one[6], config_one[7], config_one[8])
                if config_one[9] == 1:
                    data = data[1], data[2], data[0]
                if config_one[9] == 2:
                    data = data[2], data[0], data[1]
            batch_labels.append(self._one_hot_label(labels))
            batch_data.append(data)
            self.i = self.i+1
            if len(batch_labels) == self.b_no:
                break
        if len(batch_labels) == self.b_no:
            return batch_data, batch_labels
        raise StopIteration()

    def __len__(self):
        return len(self.configurations)

    def next(self):
        return self.__next__()

    @staticmethod
    def _one_hot_label(label):
        maxl = max(label)
        ret = [0]*8
        if maxl == 0:
            ret[0] = 1
        else:
            ret[int(log2(maxl))+1] = 1
        return ret


class ValidationDataGenerator:
    def __init__(self,
                 phases_init=(0.5, 6, 19),
                 signal_time_init=(0.1, 0.1, 1),
                 fault_durations_init=(0.01, 0.02, 0.03, 0.04, 0.05, 0.06),
                 sampling_frequencies_init=4096,
                 signal_frequencies_init=(50, 50, 1),
                 severities_init=(0.01, 0.99, 77),
                 rms_init=230,
                 fault_start_timepoint_init=0.04):
        """
        11469 je duzina svakog pojedinacnog uzorka kada je duzina trajanja signala 2.8 sekundi
        """
        self.b_no = 32  # batch_number
        self.configurations = []
        self.fault_start_timepoint = fault_start_timepoint_init
        types = [LLS.sag_types.A, LLS.sag_types.B, LLS.sag_types.C, LLS.sag_types.D, LLS.sag_types.E, LLS.sag_types.F, LLS.sag_types.G]
        signal_times = linspace(signal_time_init[0], signal_time_init[1], signal_time_init[2])
        sampling_frequencies = [sampling_frequencies_init]
        signal_frequencies = linspace(signal_frequencies_init[0], signal_frequencies_init[1], signal_frequencies_init[2])
        rmss = [rms_init]
        phases = linspace(phases_init[0], phases_init[1], phases_init[2])
        no_faults = product([LLS.type.NONE], signal_times, sampling_frequencies, signal_frequencies, rmss, phases)
        fault_durations = list(fault_durations_init)
        severities = linspace(severities_init[0], severities_init[1], severities_init[2])
        sag_types = product(types, signal_times, sampling_frequencies, signal_frequencies, [self.fault_start_timepoint], fault_durations, rmss, phases, severities, [0, 1, 2])
        self.configurations.extend(no_faults)
        self.configurations.extend(sag_types)
        self.configurations = permutation(self.configurations)
        self.len = len(self.configurations)
        print("Ukupan broj konfiguracija:", self.len)
        self.i = 0

    def __iter__(self):
        return self

    def __next__(self):
        batch_data = []
        batch_labels = []
        while self.i < self.len:
            config_one = self.configurations[self.i]
            if config_one[0] == LLS.type.NONE:
                data, labels = no_fault(config_one[1], config_one[2], config_one[3], config_one[4], config_one[5])
            else:
                data, labels = sag_fault(config_one[0], config_one[1], config_one[2], config_one[3], config_one[4], config_one[4]+config_one[5], config_one[6], config_one[7], config_one[8])
                if config_one[9] == 1:
                    data = data[1], data[2], data[0]
                if config_one[9] == 2:
                    data = data[2], data[0], data[1]
            batch_labels.append(self._one_hot_label(labels))
            batch_data.append(data)
            self.i = self.i+1
            if len(batch_labels) == self.b_no:
                break
        if len(batch_labels) == self.b_no:
            return batch_data, batch_labels
        raise StopIteration()

    def __len__(self):
        return len(self.configurations)

    def next(self):
        return self.__next__()

    @staticmethod
    def _one_hot_label(label):
        maxl = max(label)
        ret = [0]*8
        if maxl == 0:
            ret[0] = 1
        else:
            ret[int(log2(maxl))+1] = 1
        return ret
