import tensorflow as tf

from pathlib import Path as p
from tensorflow import keras as K
from tensorflow_addons.metrics.matthews_correlation_coefficient import MatthewsCorrelationCoefficient

from generator import TrainingDataGenerator, ValidationDataGenerator
from model import get_model

# If error ocurrs: Cannot convert a symbolic Tensor to a numpy array
# Please install manually  numpy:1.19+

root_dir = p("voltage_sag")
weight_dat_pattern = root_dir / "weights.{epoch:03d}-{val_loss:.10f}-{val_MatthewsCorrelationCoefficient:.5f}-{val_categorical_accuracy:.5f}.hdf5"
log_dir = root_dir / "log"

training_data = tf.data.Dataset.from_generator(TrainingDataGenerator,
                                               output_signature=(
                                                   tf.TensorSpec((32, 3, 410,)),
                                                   tf.TensorSpec((32, 8,))
                                               ))

validation_data = tf.data.Dataset.from_generator(ValidationDataGenerator,
                                                 output_signature=(
                                                     tf.TensorSpec((32, 3, 410,)),
                                                     tf.TensorSpec((32, 8))
                                                 ))

model = get_model()

model.compile(optimizer=K.optimizers.Adam(learning_rate=1E-4),
              loss=K.losses.CategoricalCrossentropy(),
              metrics=[MatthewsCorrelationCoefficient(8), K.metrics.FalseNegatives(0.75),
                       K.metrics.FalsePositives(0.75), K.metrics.TrueNegatives(0.75),
                       K.metrics.TruePositives(0.75), K.metrics.CategoricalAccuracy()])

model.fit(x=training_data,
          y=None,
          epochs=50,
          batch_size=32,
          validation_data=validation_data,
          use_multiprocessing=True,
          callbacks=[K.callbacks.ModelCheckpoint(weight_dat_pattern,
                                                 monitor='val_categorical_accuracy',
                                                 save_best_only=False),
                     K.callbacks.TensorBoard(log_dir=log_dir,
                                             histogram_freq=1,
                                             write_graph=True,
                                             update_freq=10),
                     K.callbacks.ReduceLROnPlateau(monitor='val_loss',
                                                   mode='min',
                                                   factor=0.1,
                                                   patience=3,
                                                   verbose=1,
                                                   min_delta=0.001,
                                                   min_lr=1E-26)])

model.summary()
