from tensorflow import keras as K
from tensorflow.keras import layers


def get_model():
    model = K.Sequential()
    model.add(layers.LSTM(128, return_sequences=True))
    model.add(layers.Conv1D(128, 7, activation='relu', data_format="channels_first"))
    model.add(layers.Conv1D(64, 7, activation='relu', data_format="channels_first"))
    model.add(layers.Dropout(0.33))
    model.add(layers.LSTM(64, return_sequences=True))
    model.add(layers.Conv1D(64, 5, activation='relu', data_format="channels_first"))
    model.add(layers.Conv1D(32, 5, activation='relu', data_format="channels_first"))
    model.add(layers.Dropout(0.33))
    model.add(layers.LSTM(32, return_sequences=False))
    model.add(layers.Flatten())
    model.add(layers.Dense(32))
    model.add(layers.Dense(16))
    # model.add(layers.Dropout(0.50))
    model.add(layers.Dense(8, activation='softmax'))
    return model